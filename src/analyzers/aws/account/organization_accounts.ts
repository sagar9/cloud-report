import { CheckAnalysisType, ICheckAnalysisResult, SeverityStatus, IResourceAnalysisResult } from "../../../types";
import { BaseAnalyzer } from "../../base";

export class OrganizationAccountsAnalyzer extends BaseAnalyzer {

    public analyze(params: any, fullReport?: any): any {
        const org_accounts = params.account_info;
        if (!org_accounts) {
            return undefined;
        }

        const curr_account_id = org_accounts.id;
        const accounts: any[] = org_accounts.Accounts;
        let isAccountInOrganization: boolean = false;
        accounts.forEach(account => {
            if(account.Id == curr_account_id) {
                isAccountInOrganization = true;
            }
        });

        const organizationAccountsSummary: ICheckAnalysisResult = { type: CheckAnalysisType.CostOptimization };
        organizationAccountsSummary.what = "AWS account is part of Organization";
        organizationAccountsSummary.why = "AWS account should be part of Organization";
        organizationAccountsSummary.recommendation = "Recommended add account in Organization";
        const analysis: IResourceAnalysisResult = {
            resourceSummary: {
                name: "AccountId",
                value: curr_account_id,
            },
        };
        if (isAccountInOrganization) {
            analysis.severity = SeverityStatus.Good;
            analysis.message = `Accounts ${curr_account_id} is in Organization`;
        } else {
            analysis.severity = SeverityStatus.Failure;
            analysis.action = "Add account in Organization";
            analysis.message = `Accounts ${curr_account_id} is not in Organization`;
        }

        organizationAccountsSummary.regions = {
            global: [analysis],
        };
        
        return { organizationAccountsSummary };
    }
}
