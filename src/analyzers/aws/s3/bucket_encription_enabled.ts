import { CheckAnalysisType, ICheckAnalysisResult, IResourceAnalysisResult, SeverityStatus } from "../../../types";
import { BaseAnalyzer } from "../../base";

export class BucketEncriptionAnalyzer extends BaseAnalyzer {

    public analyze(params: any): any {
        const allBucketEncriptionRules = params.bucket_encryption;
        if (!allBucketEncriptionRules) {
            return undefined;
        }
        const bucket_encription_used: ICheckAnalysisResult = { type: CheckAnalysisType.Security };
        bucket_encription_used.what = "Are S3 Buckets Encrypted?";
        bucket_encription_used.why = `It is better to encrypt buckets.`;
        bucket_encription_used.recommendation = "Recommended to encript buckets";
        const allBucketsAnalysis: IResourceAnalysisResult[] = [];
        for (const bucketName in allBucketEncriptionRules) {
            const bucket_analysis: IResourceAnalysisResult = {};
            bucket_analysis.resource = { bucketName, bucketLifeCycleRules: allBucketEncriptionRules[bucketName] };
            bucket_analysis.resourceSummary = {
                name: "Bucket", value: bucketName,
            };
            if (allBucketEncriptionRules[bucketName] && allBucketEncriptionRules[bucketName].length > 0) {
                bucket_analysis.severity = SeverityStatus.Good;
                bucket_analysis.message = "Bucket Encryption is configured";
            } else {
                bucket_analysis.severity = SeverityStatus.Warning;
                bucket_analysis.message = "Bucket Encryption not configured";
                bucket_analysis.action = "Configure bucket Encryption";
            }
            allBucketsAnalysis.push(bucket_analysis);
        }
        bucket_encription_used.regions = { global: allBucketsAnalysis };
        return { bucket_encription_used: bucket_encription_used };
    }
}
