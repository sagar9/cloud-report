import {
    CheckAnalysisType, ICheckAnalysisResult, IDictionary,
    IResourceAnalysisResult, SeverityStatus,
} from "../../../types";
import { BaseAnalyzer } from "../../base";


  export class UserGrounpCompareAnalyzer extends BaseAnalyzer {
    data = [
        {
            "Path": "/human-RoleType/",
            "RoleName": "200333-ReadOnly",
            "AssumeRoleTypePolicyDocument": {
              "Version": "2012-10-17",
              "Statement": [
                {
                  "Effect": "Allow",
                  "Principal": {
                    "Federated": "arn:aws:iam::962390577453:saml-provRoleTypeer/ADFS"
                  },
                  "Action": "sts:AssumeRoleTypeWithSAML",
                  "Condition": {
                    "SBringEquals": {
                      "SAML:aud": "https://signin.aws.amazon.com/saml"
                    }
                  }
                }
              ]
            },
            "MaxSessionDuration": 32400
          },
          {
            "Path": "/human-RoleType/",
            "RoleName": "200333-ReadOnly",
            "AssumeRoleTypePolicyDocument": {
              "Version": "2012-10-17",
              "Statement": [
                {
                  "Effect": "Allow",
                  "Principal": {
                    "Federated": "arn:aws:iam::962390577453:saml-provRoleTypeer/ADFS"
                  },
                  "Action": "sts:AssumeRoleTypeWithSAML",
                  "Condition": {
                    "SBringEquals": {
                      "SAML:aud": "https://signin.aws.amazon.com/saml"
                    }
                  }
                }
              ]
            },
            "MaxSessionDuration": 32400
          },
          {
            "Path": "/human-RoleType/",
            "RoleName": "200333-ReadOnly",
            "AssumeRoleTypePolicyDocument": {
              "Version": "2012-10-17",
              "Statement": [
                {
                  "Effect": "Allow",
                  "Principal": {
                    "Service": "lambda.amazonaws.com"
                  },
                  "Action": "sts:AssumeRoleTypeWithSAML",
                  "Condition": {
                    "SBringEquals": {
                      "SAML:aud": "https://signin.aws.amazon.com/saml"
                    }
                  }
                }
              ]
            },
            "MaxSessionDuration": 32400
          },
          {
            "Path": "/human-RoleType/",
            "RoleName": "200333-ReadOnly",
            "AssumeRoleTypePolicyDocument": {
              "Version": "2012-10-17",
              "Statement": [
                {
                  "Effect": "Allow",
                  "Principal": {
                    "Service": "lambda.amazonaws.com"
                  },
                  "Action": "sts:AssumeRoleTypeWithSAML",
                  "Condition": {
                    "SBringEquals": {
                      "SAML:aud": "https://signin.aws.amazon.com/saml"
                    }
                  }
                }
              ]
            },
            "MaxSessionDuration": 32400
          },
          {
            "Path": "/human-RoleType/",
            "RoleName": "200333-ReadOnly",
            "AssumeRoleTypePolicyDocument": {
              "Version": "2012-10-17",
              "Statement": [
                {
                  "Effect": "Allow",
                  "Principal": {
                    "Service": "lambda.amazonaws.com"
                  },
                  "Action": "sts:AssumeRoleTypeWithSAML",
                  "Condition": {
                    "SBringEquals": {
                      "SAML:aud": "https://signin.aws.amazon.com/saml"
                    }
                  }
                }
              ]
            },
            "MaxSessionDuration": 32400
          },
          {
            "Path": "/human-RoleType/",
            "RoleName": "200333-ReadOnly",
            "AssumeRoleTypePolicyDocument": {
              "Version": "2012-10-17",
              "Statement": [
                {
                  "Effect": "Allow",
                  "Principal": {
                    "Service": "lambda.amazonaws.com"
                  },
                  "Action": "sts:AssumeRoleTypeWithSAML",
                  "Condition": {
                    "SBringEquals": {
                      "SAML:aud": "https://signin.aws.amazon.com/saml"
                    }
                  }
                }
              ]
            },
            "MaxSessionDuration": 32400
          },
          {
            "Path": "/human-RoleType/",
            "RoleName": "200333-ReadOnly",
            "AssumeRoleTypePolicyDocument": {
              "Version": "2012-10-17",
              "Statement": [
                {
                  "Effect": "Allow",
                  "Principal": {
                    "Service": "lambda.amazonaws.com"
                  },
                  "Action": "sts:AssumeRoleTypeWithSAML",
                  "Condition": {
                    "SBringEquals": {
                      "SAML:aud": "https://signin.aws.amazon.com/saml"
                    }
                  }
                }
              ]
            },
            "MaxSessionDuration": 32400
          },
          {
            "Path": "/human-RoleType/",
            "RoleName": "200333-ReadOnly",
            "AssumeRoleTypePolicyDocument": {
              "Version": "2012-10-17",
              "Statement": [
                {
                  "Effect": "Allow",
                  "Principal": {
                    "Service": "lambda.amazonaws.com"
                  },
                  "Action": "sts:AssumeRoleTypeWithSAML",
                  "Condition": {
                    "SBringEquals": {
                      "SAML:aud": "https://signin.aws.amazon.com/saml"
                    }
                  }
                }
              ]
            },
            "MaxSessionDuration": 32400
          },
          {
            "Path": "/human-RoleType/",
            "RoleName": "202210-ReadOnly",
            "AssumeRoleTypePolicyDocument": {
              "Version": "2012-10-17",
              "Statement": [
                {
                  "Effect": "Allow",
                  "Principal": {
                    "Federated": "arn:aws:iam::962390577453:saml-provRoleTypeer/ADFS"
                  },
                  "Action": "sts:AssumeRoleTypeWithSAML",
                  "Condition": {
                    "SBringEquals": {
                      "SAML:aud": "https://signin.aws.amazon.com/saml"
                    }
                  }
                }
              ]
            },
            "MaxSessionDuration": 32400
          },
          {
            "Path": "/human-RoleType/",
            "RoleName": "205257-AdminisBrator",
            "AssumeRoleTypePolicyDocument": {
              "Version": "2012-10-17",
              "Statement": [
                {
                  "Effect": "Allow",
                  "Principal": {
                    "Federated": "arn:aws:iam::962390577453:saml-provRoleTypeer/ADFS"
                  },
                  "Action": "sts:AssumeRoleTypeWithSAML",
                  "Condition": {
                    "SBringEquals": {
                      "SAML:aud": "https://signin.aws.amazon.com/saml"
                    }
                  }
                },
                {
                  "Effect": "Allow",
                  "Principal": {
                    "AWS": "arn:aws:iam::460300312212:RoleType/human-RoleType/205257-AdminisBrator"
                  },
                  "Action": "sts:AssumeRoleType"
                }
              ]
            },
            "MaxSessionDuration": 32400
          },
          {
            "Path": "/human-RoleType/",
            "RoleName": "205257-ReadOnly",
            "AssumeRoleTypePolicyDocument": {
              "Version": "2012-10-17",
              "Statement": [
                {
                  "Effect": "Allow",
                  "Principal": {
                    "Federated": "arn:aws:iam::962390577453:saml-provRoleTypeer/ADFS"
                  },
                  "Action": "sts:AssumeRoleTypeWithSAML",
                  "Condition": {
                    "SBringEquals": {
                      "SAML:aud": "https://signin.aws.amazon.com/saml"
                    }
                  }
                }
              ]
            },
            "MaxSessionDuration": 32400
          },
          {
            "Path": "/",
            "RoleName": "205257-TestLambdaRoe",
            "AssumeRoleTypePolicyDocument": {
              "Version": "2012-10-17",
              "Statement": [
                {
                  "Effect": "Allow",
                  "Principal": {
                    "Service": "lambda.amazonaws.com"
                  },
                  "Action": "sts:AssumeRoleType"
                }
              ]
            },
            "MaxSessionDuration": 3600
          },
          {
            "Path": "/service-RoleType/",
            "RoleName": "205257-BrNuvolaAnalyticsRoleType",
            "AssumeRoleTypePolicyDocument": {
              "Version": "2012-10-17",
              "Statement": [
                {
                  "Effect": "Allow",
                  "Principal": {
                    "AWS": "arn:aws:iam::565663046794:user/service-user/205257-BrNuvolaAnalytics"
                  },
                  "Action": "sts:AssumeRoleType"
                }
              ]
            },
            "MaxSessionDuration": 3600
          }
      ]
  
    public analyze(params: any, fullReport ?: any): any {
        var result = this.data.reduce(function(acc, obj) {
            var key = obj.RoleName.match(/\d{6}/);
            var asseet_id = '';
            if (key) {
                asseet_id = key[0];
            } else {
                asseet_id = "Unknown";
            }
            console.log("Key: ", asseet_id);
            if (!acc[asseet_id]) {
                acc[asseet_id] = [];
            }
            var roleType = obj.AssumeRoleTypePolicyDocument.Statement[0].Principal['Federated'] ? "Human" : "Service";
            Object.assign(obj, {RoleType:  roleType}, {Asset_Id: asseet_id});
            acc[asseet_id].push(obj);
            return acc;
        }, {});
        var roleCount = this.countHumanAndServiceRolePerAsset(result);
        Object.keys(roleCount).forEach(function(key) {
            var totalNumOfHumanRole = roleCount[key].humanRoleCount;
            var totalNumOfServiceRole = roleCount[key].serviceRoleCount;
            var isAssetGood:boolean = false;
            if (totalNumOfHumanRole < 3 && totalNumOfServiceRole > 5) {
                isAssetGood = true;
            }
            Object.assign(roleCount[key], {IsAssetGood: isAssetGood});
        });
        console.log("Role Count Result: ", JSON.stringify(roleCount));
        const roles_group_count: ICheckAnalysisResult = { type: CheckAnalysisType.Security };
        roles_group_count.what = "Are there less service_roles for the account?";
        roles_group_count.why = "It is hard to manage security goals when there too many human roles and less service_roles as chances of mistakes increases";
        roles_group_count.recommendation = "Recommended to have 2-3 human_roles and atleast 5 service_roles per account";
        const allRegionsAnalysis: IDictionary<IResourceAnalysisResult[]> = {};
        const allRolesAnalysis: IResourceAnalysisResult[] = [];
        allRegionsAnalysis["global"] = [];
        Object.keys(roleCount).forEach(function(key) {
            const roles = roleCount[key].roles;
            for (const role of roles) {
                var asset_id = role.Asset_Id;
                const roleAnalysis: IResourceAnalysisResult = {};
                roleAnalysis.resource = role.RoleName;
                roleAnalysis.resourceSummary = {
                    name: "Role Type",
                    value: role.RoleType,
                    asset_id: role.Asset_Id,
                };
                console.log("Role: ", roleCount[key]);
                if (!roleCount[key].IsAssetGood) {
                    roleAnalysis.severity = SeverityStatus.Failure;
                    roleAnalysis.message = "Federated Role";
                    roleAnalysis.action = "Use less human roles";
                } else {
                    roleAnalysis.severity = SeverityStatus.Good;
                    roleAnalysis.message = "Service Role used";
                }
                allRegionsAnalysis["global"].push(roleAnalysis);
            }
        });
        roles_group_count.regions = allRegionsAnalysis;
        return { roles_group_count };
    }

    public countHumanAndServiceRolePerAsset(data) {
        var resJson = {};
        Object.keys(data).forEach(function(key) {
            var humanRoleCount = 0;
            var serviceRoleCount = 0;
            for (var i = 0; i < data[key].length; i++) {
                if (data[key][i].RoleType == "Human") {
                    humanRoleCount++;
                } 
                if (data[key][i].RoleType == "Service") {
                    serviceRoleCount++;
                }
            }
            resJson[key] = {
                humanRoleCount: humanRoleCount,
                serviceRoleCount: serviceRoleCount,
                roles: data[key],
            };
        });
        return resJson;
    }
}
  