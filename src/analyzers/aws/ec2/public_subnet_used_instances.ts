import {
    CheckAnalysisType, ICheckAnalysisResult, IDictionary,
    IResourceAnalysisResult, SeverityStatus,
} from "../../../types";
import { BaseAnalyzer } from "../../base";

export class PublicSubnetUsedInstancesAnalyzer extends BaseAnalyzer {
    public analyze(params: any, fullReport?: any): any {
        const instancesData = params.publicSubnetinstancesData;
        if (!instancesData) {
            return undefined;
        }
        const public_subnet_used: ICheckAnalysisResult = { type: CheckAnalysisType.Security };
        public_subnet_used.what = "Is public subnet used?";
        public_subnet_used.why = "Instances should not be in a public subnet";
        public_subnet_used.recommendation = "Instances should not be in a public subnet";
        const allRegionsAnalysis: IDictionary<IResourceAnalysisResult[]> = {};
        for (const region in instancesData) {
            const instances = instancesData[region];
            allRegionsAnalysis[region] = [];
            for (const instance of instances) {
                const tags = instance.Tags;
                var asset_id = "";
                if(typeof tags != "undefined" && tags != null && tags.length != null && tags.length > 0){
                    for (const tag of tags) {
                        if (tag.Key.indexOf("insight") > -1) {
                            asset_id = tag.Value;
                        }
                    }
                }
                const instanceAnalysis: IResourceAnalysisResult = {};
                instanceAnalysis.resource = instance;
                instanceAnalysis.resourceSummary = {
                    name: "Instance_Id",
                    value: `${instance.InstanceId}`,
                    asset_id: asset_id,
                };
                if (instance.isPublicSubnetUsed) {
                    instanceAnalysis.severity = SeverityStatus.Failure;
                    instanceAnalysis.message = "Ec2 instance is in a public subnet";
                    instanceAnalysis.action = "";
                } else {
                    instanceAnalysis.severity = SeverityStatus.Good;
                    instanceAnalysis.message = "Ec2 instance is not in a public subnet";
                }
                allRegionsAnalysis[region].push(instanceAnalysis);
            }
        }
        public_subnet_used.regions = allRegionsAnalysis;
        return { ec2_is_in_public_subnet: public_subnet_used };
    }

}
