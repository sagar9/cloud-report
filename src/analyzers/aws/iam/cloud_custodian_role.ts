import { CheckAnalysisType, ICheckAnalysisResult, IResourceAnalysisResult, SeverityStatus } from "../../../types";
import { CommonUtil } from "../../../utils";
import { BaseAnalyzer } from "../../base";
import { bool } from "aws-sdk/clients/signer";

export class CloudCustodianRoleAnalyzer extends BaseAnalyzer {
    public analyze(params: any, fullReport?: any): any {
        const allRoles = params.role_data;
        console.log(allRoles);
        const cloud_custodian_role: ICheckAnalysisResult = { type: CheckAnalysisType.CostOptimization };
        cloud_custodian_role.what = "Are there roles for Cloud Custodian?";
        cloud_custodian_role.why = `It is important to have Cloud Custodian role`;
        cloud_custodian_role.recommendation = `Recommended to have Cloud Custodian role`;
        const analysis: IResourceAnalysisResult[] = [];
        let prev_asset = '';
        for (const roleObjKey in allRoles) {
            const role = allRoles[roleObjKey];
            let isCustodianRoleExists: boolean = false;
            const tags = role.Tags;
            const roleName = role.RoleName;
            var asset_id = "";
            if(typeof tags != "undefined" && tags != null && tags.length != null && tags.length > 0) {
                for (const tag of tags) {
                    if (tag.Key.indexOf("insight") > -1) {
                        asset_id = tag.Value;
                    }
                }
            }
            const role_Analysis: IResourceAnalysisResult = {
                resourceSummary: {
                    name: "Role",
                    value: roleName,
                    asset_id: asset_id,
                },
            };
            role_Analysis.accountId = `${role.account_id}`;
            isCustodianRoleExists = this.checkIsCustodianRoleExist(allRoles);
            if (isCustodianRoleExists) {
                role_Analysis.severity = SeverityStatus.Good;
                role_Analysis.message = `Role ${roleName} is Custodian role`;
            } else {
                role_Analysis.severity = SeverityStatus.Failure;
                role_Analysis.action = "Add an Custodian Role";
                role_Analysis.message = `Role ${roleName} is not Custodian role`;
            }
            console.log("Analysis: ", JSON.stringify(role_Analysis));
            analysis.push(role_Analysis);
        }
        
        cloud_custodian_role.regions = { global: analysis };
        return { accounts_without_cloud_custodian_role: cloud_custodian_role };
    }

    private checkIsCustodianRoleExist(allRoles) {
        let isCustodianRoleExists:boolean = false;
        for (const roleObjKey in allRoles) {
            const role = allRoles[roleObjKey];
            const RoleName = role.RoleName;
            if (RoleName && RoleName.indexOf("TrCustodian") > -1) {
                isCustodianRoleExists = true;
                break;
            }
        }
        return isCustodianRoleExists;
    }
}