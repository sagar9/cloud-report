import * as flat from "flat";
import * as Analyzers from "./analyzers";
import { BaseAnalyzer } from "./analyzers/base";
import { IDictionary } from "./types";
import * as AWS from "aws-sdk";
import * as uuid from 'uuid';
import { AWSErrorHandler } from "./utils/aws";

export function analyze(collectorData: any) {
    const flatListOfAnalyzers = flat(Analyzers);
    const result: IDictionary<any> = {};
    for (const analyzerName in flatListOfAnalyzers) {
        if (!analyzerName.endsWith("Analyzer")) {
            continue;
        }
        const analyzerNameSpace = analyzerName.replace(/.[A-Za-z0-9]+$/, "");
        if (!collectorData[analyzerNameSpace]) {
            continue;
        }
        const analyzer: BaseAnalyzer = new flatListOfAnalyzers[analyzerName]();
        const data = analyzer.analyze(collectorData[analyzerNameSpace], collectorData);
        result[analyzerNameSpace] = result[analyzerNameSpace] || {};
        result[analyzerNameSpace] = Object.assign(result[analyzerNameSpace], data);
    }
    putDataInDynamo(result);
    return result;
}

/**
 * putDataInDynamo
 */
function putDataInDynamo(data) {
    var dynamodb = new AWS.DynamoDB({region: 'us-east-1'});
    var type = '';
    var resource_summary = '';
    var severity = '';
    var asset_id = '';
    try {
        for(var attributename in data){
            let resourcename = attributename;
            for(var attr in data[attributename]) {
                var resource_data = data[attributename][attr]
                for(var attr in resource_data) {
                    if(attr == 'type') {
                        type=resource_data[attr];
                    }
                    if(attr == 'regions') {
                        for(var region in resource_data[attr]) {
                            var region_data = resource_data[attr][region];
                            if (region_data.length > 0){
                                for(var i = 0; i < region_data.length; i++){
                                    resource_summary = region_data[i]['resourceSummary'];
                                    asset_id = region_data[i]['resourceSummary'].asset_id;
                                    severity = region_data[i]['severity'];
                                    let input_item: AWS.DynamoDB.Types.PutItemInput = {
                                        "TableName":"Resource_Analytics",
                                        "Item": { 
                                                    uuid :{S:uuid.v1()}, 
                                                    AssetId: {S: asset_id},
                                                    ResourceName: {S: resourcename.split(".")[1]},
                                                    ResourceSummary: {S: JSON.stringify(resource_summary)},
                                                    Type: {S: type},
                                                    Severity: {S: severity},
                                                    CreatedDate: {S:new Date().toISOString()}
                                                }
                                    };
                                    
                                    dynamodb.putItem(input_item, function(err, data) {
                                        if (err) console.log(err, err.stack); // an error occurred
                                        else     console.log(data);           // successful response
                                        
                                    });
                                }
                            }
                        }
                    }
                }
            }
        }
    } catch (error) {
        AWSErrorHandler.handle(error);
    }    
}