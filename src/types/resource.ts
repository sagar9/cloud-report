import { SeverityStatus } from "./severity";

export interface IResourceAnalysisResult {
    action?: string;
    message?: string;
    resourceSummary?: {
        name: string,
        value: string,
        asset_id?: any,
    };
    resource?: any;
    severity?: SeverityStatus;
    title?: string;
    accountId?:string;
}
