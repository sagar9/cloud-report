import * as AWS from "aws-sdk";
import { AWSErrorHandler } from "../../../utils/aws";
import { BaseCollector } from "../../base";
import { RouteTable } from "aws-sdk/clients/ec2";

export class EC2PublicSubnetInstancesCollector extends BaseCollector {
    public collect() {
        return this.getAllInstancesInPublicSubnet();
    }

    private async getAllInstancesInPublicSubnet() {
        const serviceName = "EC2";
        const ec2Regions = this.getRegions(serviceName);
        const publicSubnetinstancesData = {};

        for (const region of ec2Regions) {
            try {
                const ec2 = this.getClient(serviceName, region) as AWS.EC2;
                publicSubnetinstancesData[region] = [];
                let fetchPending = true;
                let marker: string | undefined;
                while (fetchPending) {
                    const instancesResponse: AWS.EC2.DescribeInstancesResult =
                    await ec2.describeInstances({ NextToken: marker }).promise();
                    if (instancesResponse && instancesResponse.Reservations) {
                            var ec2_instances = instancesResponse.Reservations.reduce((instancesFromReservations:
                                AWS.EC2.Instance[], reservation) => {
                                if (!reservation.Instances) {
                                    return instancesFromReservations;
                                } else {
                                    return instancesFromReservations.concat(reservation.Instances);
                                }
                            }, []);
                            publicSubnetinstancesData[region] = publicSubnetinstancesData[region].concat(ec2_instances);
                            for (var i=0; i < ec2_instances.length; i++) {
                                var params: AWS.EC2.Types.DescribeSubnetsRequest = {};
                                var subnetId: string|undefined = ec2_instances[i].SubnetId;
                                let rt_params:  AWS.EC2.Types.DescribeRouteTablesRequest  = {};
                                let  routeTable: AWS.EC2.RouteTable[] |undefined = [];
                                let routes: AWS.EC2.Route[] | undefined = [];
                                ec2_instances[i]['isPublicSubnetUsed'] = false;
                                if (subnetId) {
                                    rt_params.Filters = [{Name: "association.subnet-id", Values: [subnetId]}];
                                    await ec2.describeRouteTables(rt_params, function(err, data) {
                                        if (err) console.log(err, err.stack); // an error occurred
                                        else {
                                            if (data) {
                                                routeTable = data['RouteTables'];
                                                if (routeTable) {
                                                    for (var i=0; i<routeTable.length; i++) {
                                                        routes = routeTable[i].Routes;
                                                        if (routes) {
                                                            for (var j=0; j<routes.length; j++){
                                                                let gatewayId: string | undefined = routes[j].GatewayId;
                                                                if (gatewayId && gatewayId.indexOf("igw-") > -1 ) {
                                                                    ec2_instances[i]['isPublicSubnetUsed'] = true;
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }   
                                    }).promise();
                                }
                            }
                            marker = instancesResponse.NextToken;
                            fetchPending = marker !== undefined && marker !== null;
                    } else {
                            fetchPending = false;
                    }
                }
            } catch (error) {
                AWSErrorHandler.handle(error);
                continue;
            }
        }
        return { publicSubnetinstancesData };
    }
}
