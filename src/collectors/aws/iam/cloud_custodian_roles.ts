import * as AWS from "aws-sdk";
import { AWSErrorHandler } from "../../../utils/aws";
import { BaseCollector } from "../../base";
import { join } from "path";
import { RolesCollector } from "./roles";
import { CollectorUtil } from "../../../utils";

export class ColudCustodianRoleCollector extends BaseCollector {
    public collect() {
        return this.checkForCloudCustodianRole();
    }

    private async checkForCloudCustodianRole() {
        const roleCollector = new RolesCollector();
        roleCollector.setSession(this.getSession());
        const bucket_access_logs = {};
        try {
            const iam = this.getClient("IAM", "us-east-1") as AWS.IAM;
            let roles: AWS.IAM.Role[] = await CollectorUtil.cachedCollect(roleCollector);
            let role_data = {};
            let account_id = '';
            for (const role of roles['roles']) {
                let roleName = role.RoleName;
                let roleTagsRequest: AWS.IAM.Types.ListRoleTagsRequest = {RoleName:roleName};
                let tag_data = await iam.listRoleTags(roleTagsRequest, function(err, data) {
                    if (err) console.log(err, err.stack);
                    else {
                        role.Tags = data['Tags'];
                    }
                }).promise();
                if (account_id == '') {
                    account_id = role.Arn.split(":")[4];
                }
                role_data[roleName] = role;
                Object.assign(role_data[roleName] , {account_id: account_id});
            }
            
            return { role_data };
        } catch (error) {
            AWSErrorHandler.handle(error);
        }
    }
}
