import * as AWS from "aws-sdk";
import { AWSErrorHandler } from "../../../utils/aws";
import { BaseCollector } from "../../base";
import { AccountIdCollector } from "./id";
import { CollectorUtil } from "../../../utils";

export class OrganizationInfoCollector extends BaseCollector {
    public collect() {
        return this.getOrganizarionAccountList();
    }

    private async getOrganizarionAccountList() {
        const accountIdCollector = new AccountIdCollector();
        accountIdCollector.setSession(this.getSession());
        const curr_account_id = await CollectorUtil.cachedCollect(accountIdCollector);
        try {
            const orgClient = this.getClient("Organizations", "us-east-1") as AWS.Organizations;
            let accounts: AWS.Organizations.ListAccountsResponse = await orgClient.listAccounts().promise();
            let account_info = Object.assign(accounts, curr_account_id);
            return { account_info };
        } catch (error) {
            AWSErrorHandler.handle(error);
        }
    }
}
